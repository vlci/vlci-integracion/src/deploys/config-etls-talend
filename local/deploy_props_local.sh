#!/bin/bash


if [ -z "$1" ]; then
    echo "Usage: ./deploy_props_local.sh <directory>"
    exit 1
fi

CUSTOM_DIR=$1

python_code=/opt/etls/sc_vlci/deploys/config/local/create_exports.py
python_output=/opt/etls/sc_vlci/deploys/config/set_env.sh
python3 $python_code "$CUSTOM_DIR"

# Check if the Python script successfully generated set_env.sh
if [ -f $python_output ]; then
    source $python_output
    echo "Environment variables loaded successfully!"
    rm $python_output
    echo "Temporary set_env removed"
else
    echo "Error: set_env.sh not found. The Python script might have failed."
fi
