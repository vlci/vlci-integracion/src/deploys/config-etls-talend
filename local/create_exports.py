import glob
import os
import sys


props_dir = '/mnt/c/Talend/vlci-etls'
globals_dir = '/config-global'
specific_dir = '/config-etl-local'
output_file_full= '/opt/etls/sc_vlci/deploys/config/set_env.sh'

folder_list = [
    f'{props_dir}{globals_dir}'
]


def get_specific_folder(etl_name: str) -> str:
    return f'{props_dir}{specific_dir}/{etl_name}'

def load_properties_from_file(filepath):
    """Reads a .properties file and returns a dictionary of key-value pairs."""
    properties = {}
    with open(filepath, 'r') as file:
        for line in file:
            line = line.strip()
            if line and not line.startswith('#') and '=' in line:
                key, value = line.split('=', 1)
                properties[key.strip()] = value.strip()
    return properties


def create_env_script(folders, output_file=output_file_full):
    """Creates a shell script with export statements from the properties files."""
    with open(output_file, 'w') as out_file:
        out_file.write("#!/bin/bash\n\n")
        for folder in folders:
            properties_files = glob.glob(os.path.join(folder, '*.properties'))

            for properties_file in properties_files:
                properties = load_properties_from_file(properties_file)

                for key, value in properties.items():
                    out_file.write(f'export {key}="{value}"\n')
                    # print(f"Added {key}={value} to {output_file}")


if __name__ == "__main__":
    folder_list = folder_list

    if len(sys.argv) > 1:
        etl_name = sys.argv[1]
        specific_folder = get_specific_folder(etl_name)
        if os.path.isdir(specific_folder):
            folder_list.append(specific_folder)
        else:
            print(f"Error: {specific_folder} is not a valid directory")
            sys.exit(1)

    # Create the environment script
    print(folder_list)
    create_env_script(folder_list)
    print("Environment variables script 'set_env.sh' created.")
