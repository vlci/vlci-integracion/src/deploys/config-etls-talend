#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

function setArguments() {
    # Environment
    case $1 in
    PRE)
        SFTP_ENV=pre
        ;;
    PRO)
        SFTP_ENV=prod
        ;;
    *)
        echo "Uso: $ deploy_etl_certificates.sh (PRO|PRE) {TAGNAME} {COMMENT}" 1>&2
        echo "Example: $ deploy_etl_certificates.sh PRO 2023_S09 Sprint_2023_S09" 1>&2
        exit 2
        ;;
    esac
    ENVIRONMENT=$1
    TAGNAME=$2
    COMMENT=$3

    # Global Variables
    CERTS_SRC_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}/src/vlci-etls-secrets/certificados
    CERTS_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}/certificados
    DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/config-etls-talend/sh
}

function deploy() {
    if [ -d ${CERTS_PATH} ]; then
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - Remove ${CERTS_PATH}/* content.."
        rm -r ${CERTS_PATH}/*
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - Deploy ${CERTS_SRC_PATH}/* in ${CERTS_PATH}/."
        cp -R ${CERTS_SRC_PATH}/* ${CERTS_PATH}/.
    else
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "ERROR - ${LOG_TIMESTAMP} - ${CERTS_PATH} does not exist."; 
        RESULT=3;
    fi
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 3 ]; then
    echo "Uso: $ deploy_etl_certificates.sh (PRO|PRE) {TAGNAME} {COMMENT}"
    echo "Example: $ deploy_etl_certificates.sh PRO 2023_S09 Sprint_2023_S09"
    exit 2
fi

setArguments $1 $2 $3
source ${DEPLOY_SH_PATH}/variables.sh

${DEPLOY_SH_PATH}/get_src_git.sh ${ENVIRONMENT}
deploy

if [ "$ENVIRONMENT" = "PRO" ]; then
    ${DEPLOY_SH_PATH}/tag.sh ${ETLS_SECRETS_GIT_REPO} ${ETLS_SECRETS_GIT_REPO_NAME} ${TAGNAME} ${COMMENT}
fi

exit ${RESULT}