#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 4 ]; then
    echo "Uso: $ tag.sh {GIT_REPO} {GIT_REPO_NAME} {TAGNAME} {COMMENT}"
    echo "Example: $ tag.sh git@gitlab.com:vlci/vlci-integracion/src/vlci-etls.git vlci-etls 2023_S09 Sprint_2023_S09"
    exit 2
fi

GIT_REPO=$1
GIT_REPO_NAME=$2
TAGNAME=$3
COMMENT=$4

LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')

cd /tmp
git clone ${GIT_REPO}
echo "INFO - ${LOG_TIMESTAMP} - function tag: Repository clonned"

cd ${GIT_REPO_NAME}
if [ $(git tag -l "${TAGNAME}") ]; then
    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function tag: Tag already exists, avoid creating it again"
else
    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    git checkout main
    echo "INFO - ${LOG_TIMESTAMP} - function tag: Test branch refreshed locally"

    git tag -a ${TAGNAME} -m "${COMMENT}"
    git push origin ${TAGNAME}
    echo "INFO - ${LOG_TIMESTAMP} - function tag: Pushed tag in main branch"
fi    

rm -rf /tmp/${GIT_REPO_NAME}

exit ${RESULT}