#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

function setArguments() {
    # Environment
    case $1 in
    PRE)
        SFTP_ENV=pre
        GIT_BRANCH=test
        ;;
    PRO)
        SFTP_ENV=prod
        GIT_BRANCH=main
        ;;
    *)
        echo "Uso: $ deploy_etl_global_config.sh (PRO|PRE) {TAGNAME} {COMMENT}" 1>&2
        echo "Example: $ deploy_etl_global_config.sh PRO 2023_S09 Sprint_2023_S09" 1>&2
        exit 2
        ;;
    esac
    ENVIRONMENT=$1
    TAGNAME=$2
    COMMENT=$3

    # Global Variables
    CONFIG_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}/src/vlci-etls/config-global
    CONFIG_PATH_SECRETS=/opt/etls/sc_vlci/${ENVIRONMENT}/src/vlci-etls-secrets
    ETL_CONFIG_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}
    DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/config-etls-talend/sh
}

function deploy() {
    # Copy properties to the final path
    echo "# Global config..." >> ${ETL_CONFIG_PATH}/DelTSOL_Valencia.properties
    cat ${CONFIG_PATH}/DelTSOL_Valencia.${ENVIRONMENT}.properties > ${ETL_CONFIG_PATH}/DelTSOL_Valencia.properties
    echo  >> ${ETL_CONFIG_PATH}/DelTSOL_Valencia.properties
    echo "# The Secrets..." >> ${ETL_CONFIG_PATH}/DelTSOL_Valencia.properties
    echo  >> ${ETL_CONFIG_PATH}/DelTSOL_Valencia.properties
    cat ${CONFIG_PATH_SECRETS}/DelTSOL_Valencia.${ENVIRONMENT}.properties >> ${ETL_CONFIG_PATH}/DelTSOL_Valencia.properties
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 3 ]; then
    echo "Uso: $ deploy_etl_global_config.sh (PRO|PRE) {TAGNAME} {COMMENT}"
    echo "Example: $ deploy_etl_global_config.sh PRO 2023_S09 Sprint_2023_S09"
    exit 2
fi

setArguments $1 $2 $3
source ${DEPLOY_SH_PATH}/variables.sh

${DEPLOY_SH_PATH}/get_src_git.sh ${ENVIRONMENT}
deploy

if [ "$ENVIRONMENT" = "PRO" ]; then
    ${DEPLOY_SH_PATH}/tag.sh ${ETLS_GIT_REPO} ${ETLS_GIT_REPO_NAME} ${TAGNAME} ${COMMENT}
    ${DEPLOY_SH_PATH}/tag.sh ${ETLS_SECRETS_GIT_REPO} ${ETLS_SECRETS_GIT_REPO_NAME} ${TAGNAME} ${COMMENT}
fi

exit ${RESULT}