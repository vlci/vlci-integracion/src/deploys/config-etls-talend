#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

function setArguments() {
    # Environment
    case $1 in
    PRE)
        SFTP_ENV=pre
        GIT_BRANCH=test
        ;;
    PRO)
        SFTP_ENV=prod
        GIT_BRANCH=main
        ;;
    *)
        echo "Uso: $ deploy_etl_config.sh (PRO|PRE) {TAGNAME} {COMMENT}" 1>&2
        echo "Example: $ deploy_etl_config.sh PRO 2023_S09 Sprint_2023_S09" 1>&2
        exit 2
        ;;
    esac
    ENVIRONMENT=$1
    TAGNAME=$2
    COMMENT=$3

    # Global Variables
    CONFIG_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}/src/vlci-etls/config-etl
    CONFIG_PATH_SECRETS=/opt/etls/sc_vlci/${ENVIRONMENT}/src/vlci-etls-secrets/config-etl
    ETL_CONFIG_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}
    DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/config-etls-talend/sh
}

function deploy() {
    cd ${CONFIG_PATH}
    for f in */; do
        echo "Processing ${f} folder..";
        configure_properties ${f}
    done
}

function configure_properties() {
    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function configure_properties..."

    ETL_NAME=$1
    
    # Copy properties to the final path (if exists)
    if [ -d ${ETL_CONFIG_PATH}/${ETL_NAME}Config ]; then
        cp ${CONFIG_PATH}/${ETL_NAME}* ${ETL_CONFIG_PATH}/${ETL_NAME}Config/.

        # Copy SECRET properties to the final path (if exists)
        if [ -d ${CONFIG_PATH_SECRETS}/${ETL_NAME} ]; then
            cp ${CONFIG_PATH_SECRETS}/${ETL_NAME}* ${ETL_CONFIG_PATH}/${ETL_NAME}Config/.
        fi

        # Replace ENV and SFTP_ENV tags inside the files
        for f in ${ETL_CONFIG_PATH}/${ETL_NAME}Config/*; do
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - ${LOG_TIMESTAMP} - Processing ${f} file.."
            sed -i -e "s/{ENV}/${ENVIRONMENT}/g" ${f}
            sed -i -e "s/{SFTP_ENV}/${SFTP_ENV}/g" ${f}
        done

        # merge properties with .PRE and .PRO
        merge ${ETL_CONFIG_PATH}/${ETL_NAME}Config

    fi

}

function merge() {
    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - Merging environment properties files..."
    ETL_CONF_PATH=$1
    for f in ${ETL_CONF_PATH}/*.properties; do
        if [[ $f != *${ENVIRONMENT}.properties ]]; then
            if [ -f ${f%.*}.${ENVIRONMENT}.properties ]; then
                LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
                echo "INFO - ${LOG_TIMESTAMP} - Merging ${f%.*}.${ENVIRONMENT}.properties file.."
                echo "" >> ${f%.*}.properties
                cat ${f%.*}.${ENVIRONMENT}.properties >> ${f%.*}.properties
                rm ${f%.*}.*.properties
            fi
        fi
    done    
}


############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 3 ]; then
    echo "Uso: $ deploy_etl_config.sh (PRO|PRE) {TAGNAME} {COMMENT}"
    echo "Example: $ deploy_etl_config.sh PRO 2023_S09 Sprint_2023_S09"
    exit 2
fi

setArguments $1 $2 $3
source ${DEPLOY_SH_PATH}/variables.sh

${DEPLOY_SH_PATH}/get_src_git.sh ${ENVIRONMENT}
deploy

if [ "$ENVIRONMENT" = "PRO" ]; then
    ${DEPLOY_SH_PATH}/tag.sh ${ETLS_GIT_REPO} ${ETLS_GIT_REPO_NAME} ${TAGNAME} ${COMMENT}
    ${DEPLOY_SH_PATH}/tag.sh ${ETLS_SECRETS_GIT_REPO} ${ETLS_SECRETS_GIT_REPO_NAME} ${TAGNAME} ${COMMENT}
fi

exit ${RESULT}