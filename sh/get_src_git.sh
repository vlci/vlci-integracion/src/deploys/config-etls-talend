#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

function setArguments() {
    # Environment
    case $1 in
    PRE)
        SFTP_ENV=pre
        GIT_BRANCH=test
        ;;
    PRO)
        SFTP_ENV=prod
        GIT_BRANCH=main
        ;;
    *)
        echo "Uso: $ get_src_git.sh (PRO|PRE)" 1>&2
        exit 2
        ;;
    esac
    ENVIRONMENT=$1

    # Global Variables
    CONFIG_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}/src/vlci-etls
    CONFIG_PATH_SECRETS=/opt/etls/sc_vlci/${ENVIRONMENT}/src/vlci-etls-secrets
    DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/config-etls-talend/sh
}

function mergeTestMain() {
    cd $1 
    
    git fetch

    git switch main
    git pull
    echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMain: Main branch refreshed locally"

    git switch test
    git pull
    echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMain: Test branch refreshed locally"

    git switch main
    git merge test -m "Merge Test" --log --no-edit
    echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMain: Merged test changes to main"

    git push
    echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMain: Pushed main branch"
}

function update_properties() {
    rm -rf ${CONFIG_PATH}
    git clone -b ${GIT_BRANCH} ${ETLS_GIT_REPO} ${CONFIG_PATH}

    rm -rf ${CONFIG_PATH_SECRETS}
    git clone -b ${GIT_BRANCH} ${ETLS_SECRETS_GIT_REPO} ${CONFIG_PATH_SECRETS}

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function update_properties: Properties refreshed from the gitlab repository"    
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 1 ]; then
    echo "Uso: $ get_src_git.sh (PRO|PRE)"
    echo "Example: $ get_src_git.sh PRO"
    exit 2
fi

setArguments $1
source ${DEPLOY_SH_PATH}/variables.sh

update_properties
if [ "$ENVIRONMENT" = "PRO" ]; then
    mergeTestMain ${CONFIG_PATH}
    mergeTestMain ${CONFIG_PATH_SECRETS}
fi

exit ${RESULT}