#!/bin/bash

ETLS_GIT_REPO=git@gitlab.com:vlci/vlci-integracion/src/vlci-etls.git
ETLS_GIT_REPO_NAME=vlci-etls

ETLS_SECRETS_GIT_REPO=git@gitlab.com:vlci-secrets/vlci-etls-secrets.git
ETLS_SECRETS_GIT_REPO_NAME=vlci-etls-secrets
